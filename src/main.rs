use std::io;

const CELCIUS: &str = "C";
const FAHRENHEIT: &str = "F";

fn main() {
    loop {
        println!("Enter to what unit you want to convert");
        
        let mut unit = String::new();
        let mut user_temp = String::new();
        
        io::stdin()
            .read_line(&mut unit)
            .expect("Failed to read line");
        
        println!("what is the temps ? ");

        io::stdin()
            .read_line(&mut user_temp)
            .expect("Failed to read line");
        
        let mut temp: f64 = user_temp.trim().parse().unwrap();

        if CELCIUS.eq(unit.to_uppercase().trim()) {

            temp = convert_to_celsius(temp);
            println!("in celcius {} is {}", user_temp, temp);
        }
        else if FAHRENHEIT.eq(unit.to_uppercase().trim()){

            temp = convert_to_fahrenheit(temp);
            println!("in celcius {} is {}", user_temp, temp);
        } else {
            println!("unit is not recognize");
        }

        println!("again ? ");
        let mut retry = String::new();
        io::stdin()
            .read_line(&mut retry)
            .expect("Failed To read line");

        if retry.to_lowercase().trim() == "n" 
        || retry.to_lowercase().trim() == "no" {
            break;
        }
    }
}

fn convert_to_celsius(temp: f64) -> f64{
    return (temp - 32.0) * 0.5556;
}

fn convert_to_fahrenheit (temp: f64) -> f64{
    return (temp * 1.8) + 32.0;
}